from albumentations import Compose
from albumentations.pytorch import ToTensorV2
import albumentations as A


class TrainEffDetTransform(Compose):
    def __init__(self, img_size):
        super(TrainEffDetTransform, self).__init__([
            A.RandomSizedCrop(min_max_height=(800, 800), height=img_size, width=img_size, p=0.5),
            A.OneOf([
                A.HueSaturationValue(hue_shift_limit=0.2, sat_shift_limit=0.2,
                                     val_shift_limit=0.2, p=0.9),
                A.RandomBrightnessContrast(brightness_limit=0.2,
                                           contrast_limit=0.2, p=0.9),
            ], p=0.9),
            A.ToGray(p=0.01),
            A.HorizontalFlip(p=0.5),
            A.VerticalFlip(p=0.5),
            A.Resize(height=256, width=256, p=1),
            A.Cutout(num_holes=8, max_h_size=64, max_w_size=64, fill_value=0, p=0.5),
            ToTensorV2(p=1.0),
        ],
            p=1.0,
            bbox_params=A.BboxParams(
                format='pascal_voc',
                min_area=0,
                min_visibility=0,
                label_fields=['labels']
            ))


class ValidEffDetTransform(Compose):
    def __init__(self, img_size):
        super(ValidEffDetTransform, self).__init__([
            A.Resize(height=img_size, width=img_size, p=1.0),
            ToTensorV2(p=1.0),
        ],
            p=1.0,
            bbox_params=A.BboxParams(
                format='pascal_voc',
                min_area=0,
                min_visibility=0,
                label_fields=['labels']
            ))


class TestEffDetTransform(Compose):
    def __init__(self, img_size):
        super(TestEffDetTransform, self).__init__([
            A.Resize(height=img_size, width=img_size, p=1.0),
            ToTensorV2(p=1.0),
        ], p=1.0)
