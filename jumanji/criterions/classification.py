from typing import Optional

import torch
import torch.nn.functional as F
from torch.nn.modules.loss import _Loss
from torch.nn.modules import CrossEntropyLoss


class FocalLoss(_Loss):
    def __init__(self, alpha=0.5, gamma=2, size_average=None, reduce=None, reduction: str = 'mean'):
        super(FocalLoss, self).__init__(size_average, reduce, reduction)
        self.alpha = torch.Tensor([1 - alpha, alpha])
        self.gamma = gamma

    def forward(self, inputs, targets):
        BCE_loss = F.binary_cross_entropy(inputs, targets.float(), reduction="none")
        alphas = self.alpha.to(targets.device).gather(0, targets.data.view(-1))
        pt = torch.exp(-BCE_loss)
        F_loss = alphas * (1 - pt) ** self.gamma * BCE_loss
        if self.reduction == "mean":
            return F_loss.mean()
        else:
            return F_loss.sum()


class CrossEntropy(CrossEntropyLoss):
    def __init__(self, weight: Optional[torch.Tensor] = None, size_average=None, ignore_index: int = -100,
                 reduce=None, reduction: str = 'mean') -> None:
        if weight:
            weight = torch.Tensor(weight)
        super(CrossEntropy, self).__init__(weight, size_average, reduce, reduction)
        self.ignore_index = ignore_index

    def forward(self, input: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        weight = self.weight.to(target.device) if self.weight is not None else None
        return F.cross_entropy(input, target, weight=weight,
                               ignore_index=self.ignore_index, reduction=self.reduction)
