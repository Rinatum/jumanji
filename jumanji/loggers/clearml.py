import os

from clearml import Task
from clearml.storage.manager import StorageManager
from pytorch_lightning.loggers import TensorBoardLogger

from jumanji.registry import LOGGERS


@LOGGERS.register_class
class ClearMLLogger(TensorBoardLogger):
    def __init__(self, *args, default_root_dir=None, **kwargs):
        if not default_root_dir:
            default_root_dir = os.getcwd()
        super(ClearMLLogger, self).__init__(save_dir=default_root_dir, name='lightning_logs')
        self.task = Task.init(*args, **kwargs)
        self.storage_manager = StorageManager