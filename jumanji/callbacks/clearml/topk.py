from collections import defaultdict
from typing import Any

import cv2
import numpy as np
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.callbacks import Callback

from jumanji.registry import CALLBACKS


@CALLBACKS.register_class
class TopKVisualize(Callback):
    def __init__(self, best_k=10, worst_k=10):
        self.best_k = best_k
        self.worst_k = worst_k

        self.outputs = defaultdict(list)

    def on_train_epoch_end(self, trainer, pl_module: LightningModule, outputs: Any) -> None:
        self.save_top_k(pl_module.data[0].dataset, trainer, "train")
        self.reset_outputs()

    def on_validation_epoch_end(self, trainer, pl_module: LightningModule) -> None:
        self.save_top_k(pl_module.data[1].dataset, trainer, "eval")
        self.reset_outputs()

    def on_train_batch_end(
            self,
            trainer,
            pl_module: LightningModule,
            outputs: Any,
            batch: Any,
            batch_idx: int,
            dataloader_idx: int
    ) -> None:
        self.__on_batch_end(trainer,
                            pl_module,
                            outputs,
                            batch,
                            batch_idx,
                            dataloader_idx)

    def on_validation_batch_end(
            self,
            trainer,
            pl_module: LightningModule,
            outputs: Any,
            batch: Any,
            batch_idx: int,
            dataloader_idx: int
    ) -> None:
        self.__on_batch_end(trainer,
                            pl_module,
                            outputs,
                            batch,
                            batch_idx,
                            dataloader_idx)

    def on_test_batch_end(
            self,
            trainer,
            pl_module: LightningModule,
            outputs: Any,
            batch: Any,
            batch_idx: int,
            dataloader_idx: int
    ) -> None:
        self.__on_batch_end(trainer,
                            pl_module,
                            outputs,
                            batch,
                            batch_idx,
                            dataloader_idx)

    def __on_batch_end(
            self,
            trainer,
            pl_module: LightningModule,
            outputs: Any,
            batch: Any,
            batch_idx: int,
            dataloader_idx: int
    ) -> None:
        if type(outputs) == list:
            return None
        probas = outputs["y_pred_probas"]
        y_pred = outputs["y_pred"]
        y_true = outputs["y_true"]
        idx = outputs["idx"]

        # print(y_pred.size())
        self.__pick_top(probas, y_pred, y_true, idx)

    def __pick_top(self, probas, y_pred, y_true, idx):
        correct = y_pred == y_true
        incorrect = y_pred != y_true
        correct_probas = probas[correct]
        incorrect_probas = probas[incorrect]

        if correct_probas.nelement():
            best_proba, best_id = correct_probas.max(0)
            best_idx = idx[correct][best_id]
            best_y_pred = y_pred[correct][best_id]
            self.outputs["best_idx"].append(best_idx.item())
            self.outputs["best_proba"].append(best_y_pred.item())
            self.outputs["best_y_pred"].append(best_y_pred.item())

        if incorrect_probas.nelement():
            worst_proba, worst_id = incorrect_probas.min(0)
            worst_idx = idx[incorrect][worst_id]
            worst_y_pred = y_pred[incorrect][worst_id]
            worst_y_true = y_true[incorrect][worst_id]
            self.outputs["worst_idx"].append(worst_idx.item())
            self.outputs["worst_proba"].append(worst_proba.item())
            self.outputs["worst_y_pred"].append(worst_y_pred.item())
            self.outputs["worst_y_true"].append(worst_y_true.item())

    def save_top_k(self, dataset, trainer, prefix) -> None:
        worst_order = np.argsort(self.outputs["worst_proba"])
        worst_idx = np.array(self.outputs["worst_idx"])[worst_order][-self.worst_k:]

        best_order = np.argsort(self.outputs["best_proba"])
        best_idx = np.array(self.outputs["best_idx"])[best_order][-self.best_k:]

        best_images = np.array([cv2.resize(dataset.get_raw(idx)['input'], (224, 224)) for idx in best_idx])
        worst_images = np.array([cv2.resize(dataset.get_raw(idx)['input'], (224, 224)) for idx in worst_idx])

        if best_images.shape[0] != 0:
            trainer.logger.experiment.add_images(
                tag=f'{prefix} best images', img_tensor=best_images, global_step=trainer.global_step,
                dataformats="NHWC")

        if worst_images.shape[0] != 0:
            trainer.logger.experiment.add_images(
                tag=f'{prefix} worst images', img_tensor=worst_images, global_step=trainer.global_step,
                dataformats="NHWC")

    def reset_outputs(self):
        self.outputs = defaultdict(list)
