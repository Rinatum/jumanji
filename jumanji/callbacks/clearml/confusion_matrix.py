from pytorch_lightning.callbacks import Callback

from jumanji.registry import CALLBACKS


@CALLBACKS.register_class
class ShowConfusionMatrix(Callback):
    def __init__(self, monitor):
        self.monitor = monitor
        self.prefix = ""

    def on_train_epoch_start(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        self.prefix = "train"

    def on_validation_epoch_start(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        self.prefix = "valid"

    def on_test_epoch_start(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        self.prefix = "test"

    def on_epoch_end(self, trainer: 'pl.Trainer', pl_module: 'pl.LightningModule') -> None:
        """Called when either of train/val/test epoch ends."""
        confmat = pl_module.metrics.__getattr__(self.monitor).compute().cpu().detach().numpy()
        self.__show_confusion_matrix(trainer, confmat)

    def __show_confusion_matrix(self, trainer, confusion_matrix):
        trainer.logger.task.logger.report_confusion_matrix(
            f"confusion_matrix/{self.prefix}",
            series=" ",
            iteration=trainer.current_epoch,
            matrix=confusion_matrix
        )