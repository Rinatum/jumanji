from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.callbacks import Callback

from jumanji.registry import CALLBACKS


@CALLBACKS.register_class
class UploadConfig(Callback):
    def on_fit_start(self, trainer, pl_module: LightningModule) -> None:
        print("Upload experiment configuration")
        try:
            trainer.logger.task.connect_configuration(pl_module.config)
        except AttributeError:
            print("UploadConfig Callback works only with ClearMLLogger")