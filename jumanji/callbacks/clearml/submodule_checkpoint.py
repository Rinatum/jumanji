import os
from pytorch_lightning.callbacks import ModelCheckpoint, Callback
from jumanji.registry import CALLBACKS
from clearml.model import OutputModel


@CALLBACKS.register_class
class SubmoduleCheckpoint(ModelCheckpoint):
    def __init__(self, monitor, save_top_k, save_weights_only, submodule_names):
        super().__init__(monitor=monitor, save_top_k=save_top_k, save_weights_only=save_weights_only)
        self.submodule_names = submodule_names

    def _do_save(self, trainer: 'pl.Trainer', filepath: str) -> None:
        # make paths
        self._fs.makedirs(os.path.dirname(filepath), exist_ok=True)

        model = trainer.lightning_module
        all_weights = model.state_dict()

        for module_name in self.submodule_names:
            current_model_weights = {}
            for weight_key in all_weights.keys():
                if 'models.' + module_name in weight_key:
                    current_model_weights['.'.join(weight_key.split('.')[2:])] = all_weights[weight_key]
            trainer.accelerator.save_checkpoint(current_model_weights, filepath)
            # print(filepath)
