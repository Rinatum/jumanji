from typing import Callable, List
import albumentations
import torchmetrics
from albumentations.core.transforms_interface import BasicTransform
from albumentations.core.composition import BaseCompose
from albumentations.pytorch import transforms
from torch.nn import Module
from torch.nn.modules import pooling, linear
from torch import optim
from torch.optim import lr_scheduler
from torch.optim.lr_scheduler import _LRScheduler
from torch.nn.modules import loss
from torch.nn.modules.loss import _Loss
from torch.utils.data.dataset import Dataset
from torchvision import datasets
from pytorch_lightning import LightningModule
from torchmetrics.metric import Metric
from pytorch_lightning import loggers
from pytorch_lightning.loggers import LightningLoggerBase
from pytorch_lightning import callbacks
from pytorch_lightning.callbacks.base import Callback
from torch.utils.data import sampler
from torch.utils.data.sampler import Sampler
from .registry import Registry


def is_sub_class_checker(parents: List[type]) -> Callable[[type], bool]:
    def is_sub_class(class_: type) -> bool:
        for p in parents:
            if issubclass(class_, p):
                return True
        return False

    return is_sub_class


MODELS = Registry("models", is_sub_class_checker([Module]), initial_modules=[pooling, linear])
DATASETS = Registry("datasets", is_sub_class_checker([Dataset]), initial_modules=[datasets])
PIPELINES = Registry("pipelines", is_sub_class_checker([LightningModule]))
OPTIMIZERS = Registry("optimizers", is_sub_class_checker([optim.Optimizer]),
                      initial_modules=[optim])
SCHEDULERS = Registry("schedulers", is_sub_class_checker([_LRScheduler]),
                      initial_modules=[lr_scheduler])
CRITERIONS = Registry("criterions", is_sub_class_checker([_Loss]),
                      initial_modules=[loss])
METRICS = Registry("metrics", is_sub_class_checker([Metric]),
                   initial_modules=[torchmetrics])
LOGGERS = Registry("loggers", is_sub_class_checker([LightningLoggerBase]),
                   initial_modules=[loggers])
TRANSFORMS = Registry("transforms", is_sub_class_checker([BasicTransform, BaseCompose]),
                      initial_modules=[albumentations, transforms])
CALLBACKS = Registry("callbacks", is_sub_class_checker([Callback]),
                     initial_modules=[callbacks])
SAMPLERS = Registry("samplers", is_sub_class_checker([Sampler]),
                    initial_modules=[sampler])

torchmetrics.F1