from typing import Callable, Optional, List


class Registry:
    def __init__(self, name: str, checker: Callable[[type], bool], initial_modules: Optional[List] = []):
        self._name = name
        self._class_dict = dict()
        self.checker = checker
        for initial_module in initial_modules:
            for k, v in initial_module.__dict__.items():
                if isinstance(v, type) and checker(v):
                    self._class_dict[k] = v

    def __repr__(self):
        format_str = self.__class__.__name__
        format_str += f'(name={self._name}, items={list(self._class_dict)})'
        return format_str

    def __contains__(self, item):
        return item in self._class_dict

    def __getitem__(self, key):
        return self.get(key)

    @property
    def name(self):
        return self._name

    @property
    def class_dict(self):
        return self._class_dict

    def get(self, key):
        result = self._class_dict.get(key, None)
        if result is None:
            raise KeyError(f'{key} is not in the {self._name} registry')
        return result

    def _register_class(self, _class):
        if not (isinstance(_class, type) or callable(_class)):
            raise TypeError(f'class must be a class, but got {type(_class)}')
        if not self.checker(_class):
            raise TypeError(f'class must match the checker\'s conditions, but it returned false')
        class_name = _class.__name__
        if class_name in self._class_dict:
            raise KeyError(f'{class_name} is already registered in {self.name}')
        self._class_dict[class_name] = _class

    def register_class(self, cls):
        self._register_class(cls)
        return cls
