import torch
from .base import Pipeline

IMG_SIZE = 256


class EffDetPipeline(Pipeline):
    def forward(self, x):
        return self.models.model(x)

    def training_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def validation_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def test_step(self, batch, batch_nb):
        return self._step(batch, batch_nb)

    def _step(self, batch, batch_idx):
        images, targets = batch
        targets = [{k: v for k, v in t.items()} for t in targets]
        # separate losses
        images = torch.stack(images).float()
        targets2 = {
            "bbox": [target["boxes"].float() for target in targets],
            "cls": [target["labels"].float() for target in targets],
            "image_id": torch.tensor(
                [target["image_id"] for target in targets]
            ).float(),
            "img_scale": torch.tensor(
                [target["img_scale"] for target in targets]
            ).float(),
            "img_size": torch.tensor(
                [(IMG_SIZE, IMG_SIZE) for _ in targets]
            ).float()
        }
        losses_dict = self.models[0](images, targets2)

        return {"loss": losses_dict["loss"], "log": losses_dict}

    def training_epoch_end(self, outputs):
        return self._epoch_end(outputs, "train")

    def validation_epoch_end(self, outputs):
        return self._epoch_end(outputs, "valid")

    def test_epoch_end(self, outputs):
        return self._epoch_end(outputs, "test")

    def _epoch_end(self, outputs, mode):
        pass