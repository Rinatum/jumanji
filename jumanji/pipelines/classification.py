from .base import LoggablePipeline

import torch
from torch.nn.functional import softmax

from jumanji.registry import PIPELINES


@PIPELINES.register_class
class MetricLearningPipeline(LoggablePipeline):
    def forward(self, x):
        features = self.models.backbone(x)
        pooled = self.models.pooler(features)
        embedded = self.models.neck(pooled)
        return embedded

    def _step(self, batch, batch_idx):
        x, y = batch["input"], batch["target"]
        features = self.models.backbone(x)
        pooled = self.models.pooler(features)
        embedded = self.models.neck(pooled)
        logits = self.models.head(embedded)
        probas = softmax(logits, dim=1)
        y_pred_probas, y_pred = probas.max(dim=1)
        loss = self.criterions.classification(probas, y.squeeze(-1))
        conf_mat = self.metrics.confusion_matrix(y_pred, y)

        return {"y_true": y,
                "logits": logits,
                "y_pred_probas": y_pred_probas,
                "y_pred": y_pred,
                "loss": loss,
                "conf_mat": conf_mat,
                "idx": batch["idx"]}
