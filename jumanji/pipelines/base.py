import torch
from pytorch_lightning import LightningModule
from torchmetrics import ConfusionMatrix


class Pipeline(LightningModule):
    def __init__(
            self,
            models,
            optimizers,
            schedulers,
            criterions,
            metrics,
            data,
    ):
        super().__init__()
        self.config = None
        self.models = models
        self.optimizers = optimizers
        self.schedulers = schedulers
        self.criterions = criterions
        self.metrics = metrics
        self.data = data

    def set_configuration(self, config):
        self.config = config

    def configure_optimizers(self):
        return self.optimizers, self.schedulers

    def train_dataloader(self):
        return self.data[0]

    def val_dataloader(self):
        return self.data[1]

    def test_dataloader(self):
        return self.data[2]

    def training_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def validation_step(self, batch, batch_idx):
        return self._step(batch, batch_idx)

    def test_step(self, batch, batch_nb):
        return self._step(batch, batch_nb)

    def training_epoch_end(self, outputs):
        return self._epoch_end(outputs, "train")

    def validation_epoch_end(self, outputs):
        return self._epoch_end(outputs, "valid")

    def test_epoch_end(self, outputs):
        return self._epoch_end(outputs, "test")

    def collect_criterions(self, outputs):
        loss = torch.Tensor([0])

        for criterion_name, criterion in self.criterions.items():
            forward_params = {forward_field: outputs[output_field]
                              for forward_field, output_field in criterion.forward_params.items()}
            outputs[criterion_name] = criterion.loss(**forward_params)
            loss += criterion.weight * outputs[criterion_name]

        outputs["loss"] = loss

    def collect_metrics(self, outputs):
        for metric_name, metric in self.metrics.items():
            update_params = {forward_field: outputs[output_field]
                             for forward_field, output_field in metric.update_params.items()}
            outputs[metric_name] = metric.metric(**update_params)


class LoggablePipeline(Pipeline):
    def _epoch_end(self, outputs, mode):
        logs = self._collect_logs(outputs, mode)
        self.log_dict(logs, logger=True)

    def _collect_logs(self, outputs, mode):
        logs = {}
        logs.update(self._collect_metrics_logs(outputs, mode))
        logs.update(self._collect_criterions_logs(outputs, mode))

        return logs

    def _collect_criterions_logs(self, outputs, mode):
        logs = {}
        # TODO: Universal loss log collection
        for loss_name, loss in zip(self.criterions._fields, self.criterions):
            logs[f"{mode}_{loss_name}_{loss.__class__.__name__}"] = torch.stack([x["loss"] for x in outputs]).mean()

        return logs

    def _collect_metrics_logs(self, outputs, mode):
        y_pred = []
        y_true = []
        for batch_output in outputs:
            for _y_pred in batch_output["y_pred"]:
                y_pred.append(_y_pred)
            for _y_true in batch_output["y_true"]:
                y_true.append(_y_true)

        y_pred = torch.tensor(y_pred, dtype=torch.int64)
        y_true = torch.tensor(y_true, dtype=torch.int64)

        logs = {}
        for metric_name, metric in self.metrics._modules.items():
            # TODO: Skip all metrics with non scalar output
            if type(metric) is not ConfusionMatrix:
                logs[f"{mode}_{metric_name}_{metric.__class__.__name__}"] = metric.update(y_pred, y_true)
                logs[f"{mode}_{metric_name}_{metric.__class__.__name__}"] = metric.compute()

        return logs
