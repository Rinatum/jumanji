import torch
import torch.nn.functional as F
from torch import nn, Tensor

from jumanji.registry import MODELS


class AbstractHead(nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features

    def init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)


@MODELS.register_class
class LinearHead(AbstractHead):
    def __init__(self, in_features, out_features, drop_rate=0.0, normalize=False):
        super().__init__(in_features, out_features)
        self.drop_rate = drop_rate
        self.normalize = normalize
        self.linear = nn.Linear(in_features, out_features)
        self.init_weights()

    def forward(self, x: Tensor) -> Tensor:
        if self.drop_rate > 0.:
            x = F.dropout(x, p=self.drop_rate, training=self.training)

        x = self.linear(x)

        if self.normalize:
            x = F.normalize(x, p=2, dim=-1)

        return x


@MODELS.register_class
class ClassificationHead(AbstractHead):
    def __init__(self, in_features, num_classes, drop_rate=0.0):
        super().__init__(in_features, num_classes)
        self.num_classes = num_classes
        self.drop_rate = drop_rate
        self.classifier = nn.Linear(in_features, num_classes)
        self.init_weights()

    def forward(self, x: Tensor) -> Tensor:
        if self.drop_rate > 0.:
            x = F.dropout(x, p=self.drop_rate, training=self.training)
        x = self.classifier(x)
        if self.num_classes == 1:
            x = x[:, 0]

        return x
