from collections import namedtuple

import torch
from torch.nn import Module
from torch.utils.data import WeightedRandomSampler, DataLoader
from pytorch_lightning import Trainer
from albumentations.core.composition import BaseCompose
from clearml import Task

from jumanji.config import PipelineConfig, TrainerConfig
from jumanji.registry import PIPELINES, MODELS, CRITERIONS, OPTIMIZERS, METRICS, CALLBACKS, LOGGERS, TRANSFORMS, \
    DATASETS


def get_model_weights(model_name, load_pretrained):
    pretrained_model_name = load_pretrained[model_name]['pretrained_model_name']
    task_id = load_pretrained[model_name]['task_id']
    task = Task.get_task(task_id=task_id)
    model_tag = task.get_models()['output'][-1]
    all_weights_path = model_tag.get_weights()

    all_weights = torch.load(all_weights_path)['state_dict']

    current_model_weights = {}
    for weight_key in all_weights.keys():
        if 'models.' + pretrained_model_name in weight_key:
            current_model_weights['.'.join(weight_key.split('.')[2:])] = all_weights[weight_key]

    if len(current_model_weights) == 0:
        raise NameError(f"{pretrained_model_name} is not in the specified Task ID model's weights")

    return current_model_weights


def parse_pipeline(pipeline_config):
    pipeline_config = pipeline_config.copy()
    pipeline_config = PipelineConfig(**pipeline_config)

    load_pretrained = {}
    for model_name, model in pipeline_config.models.items():
        if len(model.pretrained) != 0:
            pretrained_model_name = model.pretrained['model_name'] if 'model_name' in model.pretrained else model_name
            if 'task_id' not in model.pretrained:
                raise NameError('Task ID is not specified')
            task_id = model.pretrained['task_id']
            load_pretrained[model_name] = {
                'pretrained_model_name': pretrained_model_name,
                'task_id': task_id
            }

    freeze = {}
    for model_name, model in pipeline_config.models.items():
        if model.freeze:
            freeze[model_name] = True

    parsed_models = {
        model_name: MODELS[model.name](**model.params)
        for model_name, model in pipeline_config.models.items()
    }
    models = Module()
    for model_name, model in parsed_models.items():
        if model_name in load_pretrained:
            weights = get_model_weights(model_name, load_pretrained)
            model.load_state_dict(weights)
            print(f'Loaded pretrained weights for {model_name} from '
                  f'{load_pretrained[model_name]["pretrained_model_name"]} from the task '
                  f'{load_pretrained[model_name]["task_id"]}')

        if model_name in freeze:
            for param in model.parameters():
                param.requires_grad = False
            print(f'{model_name} has been frozen')

        models.__setattr__(model_name, model)

    criterions = {
        criterion_name: CRITERIONS[criterion.name](**criterion.params)
        for criterion_name, criterion in pipeline_config.criterions.items()
    }

    criterions = namedtuple("criterions", criterions.keys())(**criterions)

    parsed_metrics = {
        metric_name: METRICS[metric.name](**metric.params)
        for metric_name, metric in pipeline_config.metrics.items()
    }
    metrics = Module()
    for metric_name, metric in parsed_metrics.items():
        metrics.__setattr__(metric_name, metric)

    optimizers = [OPTIMIZERS[optimizer.name](models.parameters(), **optimizer.params)
                  for optimizer in pipeline_config.optimizers]

    schedulers = [OPTIMIZERS[scheduler.name](models.parameters(), **scheduler.params)
                  for scheduler in pipeline_config.schedulers]

    data = parse_data(pipeline_config.data)
    pipeline = PIPELINES[pipeline_config.name](models, optimizers, schedulers, criterions, metrics, data)
    return pipeline


def parse_data(data):
    train_loader = configure_data(data.train)
    eval_loader = configure_data(data.eval)
    test_loader = configure_data(data.test) if data.test else None

    return train_loader, eval_loader, test_loader


def configure_data(data_params):
    transform = data_params.dataset.params.get("transform")

    if transform:
        data_params.dataset.params["transform"] = parse_transforms(data_params.dataset.params["transform"])
    augment = data_params.dataset.params.get("augment")
    if augment:
        data_params.dataset.params["augment"] = parse_transforms(augment)
    dataset = DATASETS[data_params.dataset.name](**data_params.dataset.params)

    # TODO: Add universal sampler
    sampler = data_params.dataloader.params.get("sampler")
    if sampler:
        data_params.dataloader.params["sampler"] = prepare_sampler(dataset)

    return DataLoader(dataset, **data_params.dataloader.params)


def parse_transforms(tranforms_yaml):
    transform_type = type(tranforms_yaml)
    if transform_type == list:
        transform_class = TRANSFORMS["Compose"]
        transforms = [parse_transforms(transform) for transform in tranforms_yaml]
        return transform_class(transforms=transforms)
    else:
        transform_name = tranforms_yaml.pop("name")
        transform_class = TRANSFORMS[transform_name]

    if issubclass(transform_class, BaseCompose):
        params = tranforms_yaml["params"]
        transforms = params.pop("transforms")
        transforms = [parse_transforms(transform) for transform in transforms]
        return transform_class(transforms=transforms, **params)
    else:
        return transform_class(**tranforms_yaml.get("params", {}))


def parse_trainer(trainer_config):
    trainer_params = TrainerConfig(**trainer_config)
    if trainer_params.callbacks:
        trainer_params.callbacks = [CALLBACKS[callbacks_params.name](**callbacks_params.params)
                                    for callbacks_params in trainer_params.callbacks]

    logger_name = trainer_params.logger.name

    trainer_params.logger = LOGGERS[logger_name](**trainer_params.logger.params,
                                                     default_root_dir=trainer_params.default_root_dir)

    trainer = Trainer(**trainer_params.dict())

    return trainer


def prepare_sampler(dataset):
    class_count = dataset.csv.label.value_counts()
    class_count = [class_count[label] for label in range(len(class_count))]

    target_list = torch.LongTensor(dataset.csv.label)
    target_list = target_list[torch.randperm(len(target_list))]

    class_weights = 1 - torch.Tensor(class_count) / sum(class_count)

    class_weights_all = class_weights[target_list]

    weighted_sampler = WeightedRandomSampler(
        weights=class_weights_all,
        num_samples=len(class_weights_all),
        replacement=True
    )

    return weighted_sampler